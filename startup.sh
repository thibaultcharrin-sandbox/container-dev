#!/bin/bash

IMAGE=custom-rhel
TAG=latest
CONTAINER_NAME=env-default
PROXY=""

if [[ $1 != "" ]]; then CONTAINER_NAME=$1; fi

function build_env() {
	echo -e "\n*** Building RHEL Environment ***"
	if [[ $(docker images | grep -cE "${IMAGE}.*${TAG}") != 0 ]]; then
		docker rmi -f "${IMAGE}:${TAG}"
	fi
	docker build -t "${IMAGE}:${TAG}" . $PROXY
}

function launch_env() {
	echo -e "\n*** Launching Environment ***"
	docker run -d --name "$CONTAINER_NAME" "${IMAGE}:${TAG}"
	sleep 1
	docker exec -it "$CONTAINER_NAME" /bin/bash
}

# STEPS
if [[ $(docker ps -a | grep -c "$CONTAINER_NAME") == 0 ]]; then
	build_env
	launch_env
else
	docker start "$CONTAINER_NAME"
	docker exec -it "$CONTAINER_NAME" /bin/bash
fi
