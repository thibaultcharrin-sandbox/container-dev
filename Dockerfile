FROM quay.io/devspaces/theia-dev-rhel8:next
WORKDIR /home/theia-dev
ADD requirements.txt  ./
RUN dnf install -y https://dl.fedoraproject.org/pub/epel/epel-release-latest-8.noarch.rpm
RUN dnf install -y sudo util-linux-user 
RUN dnf install -y git vim curl wget zsh htop neofetch python39
RUN pip3 install --upgrade pip
RUN pip install -r requirements.txt

RUN git clone https://gitlab.com/devops-collab/linux-config-bootstrap.git
RUN rm requirements.txt
