#!/bin/bash

CONTAINER_NAME="env-default"

if [[ $1 != "" ]]; then CONTAINER_NAME=$1; fi
docker stop "$CONTAINER_NAME"
